
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class GenerateTimeTable {
    ArrayList<String> days;
    ArrayList<Integer> hoursPerDay;
    ArrayList<String> subject;
    ArrayList<String> teacherName;
    ArrayList<String> classroom;
    ArrayList<Integer> timePerSubject;
    TimetableInfo ttInfo;
    
    GenerateTimeTable(TimetableInfo ttInfo){
        this.ttInfo = ttInfo;
        days = ttInfo.getDays();
        hoursPerDay = ttInfo.getHoursPerDay();
        subject = ttInfo.getSubject();
        teacherName = ttInfo.getTeacherName();
        classroom = ttInfo.getClassroom();
        timePerSubject = ttInfo.getTimePerSubject();
    }
    
    DefaultTableModel generateTt(){
        int nRow = Collections.max(hoursPerDay);
        int nCol = days.size();
        Object[][] tableData = new Object[nRow][nCol];
        
        for(int i=0;i<days.size();i++){
            int noOfHours = hoursPerDay.get(i);
            int j=0;
            while(noOfHours!=0){
                Random rand = new Random();
                int num = rand.nextInt(subject.size());
                int remainingTime = timePerSubject.get(num);
                
                if(remainingTime!=0 && !isRepeat(tableData,i,j,subject.get(num))){
                    tableData[j][i] = subject.get(num);
                    timePerSubject.set(num, remainingTime-1);
                    noOfHours--;
                    j++;
                }
                
            }
        }
        
        String[] colHeads = new String[days.size()];
        for(int i=0;i<days.size();i++){
            colHeads[i] = days.get(i);
        }
        
        DefaultTableModel myModel = new DefaultTableModel(tableData, colHeads){
            @Override
            public boolean isCellEditable(int row, int column){
                return false;
            }
        };
        return myModel;
        
    }
    
    boolean isRepeat(Object[][] tableData, int i, int j, String sub){
        for(int idx=0;idx<j;idx++){
            if(tableData[idx][i].equals(sub))
                return true;
        }
        return false;
    }
    
    void validate(){
        if(days.size()!= hoursPerDay.size())
            JOptionPane.showMessageDialog(new JFrame(), "Number of days and hours per day dont match!");
        
        int totalHoursAWeek=0;
        for(int i:hoursPerDay)
            totalHoursAWeek+=i;
        
        int totalSubjectHours = 0;
        for(int i:timePerSubject)
            totalSubjectHours+=i;
        
        if(totalHoursAWeek!=totalSubjectHours)
            JOptionPane.showMessageDialog(new JFrame(), "Total working hours and total subject hours do not match!");
    }
}
