
import java.util.ArrayList;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;


public class TimetableGenerator_UI extends javax.swing.JFrame {
TimetableInfo ttInfo;
   
    public TimetableGenerator_UI() {
        initComponents();
        ttInfo = new TimetableInfo();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        Tue_jCB = new javax.swing.JCheckBox();
        Mon_jCB = new javax.swing.JCheckBox();
        Wed_jCB = new javax.swing.JCheckBox();
        Thu_jCB = new javax.swing.JCheckBox();
        Fri_jCB = new javax.swing.JCheckBox();
        Sat_jCB = new javax.swing.JCheckBox();
        Sun_jCB = new javax.swing.JCheckBox();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        MonHours_jTF = new javax.swing.JTextField();
        TueHours_jTF = new javax.swing.JTextField();
        WedHours_jTF = new javax.swing.JTextField();
        ThuHours_jTF = new javax.swing.JTextField();
        FriHours_jTF = new javax.swing.JTextField();
        SatHours_jTF = new javax.swing.JTextField();
        SunHours_jTF = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        addBtn = new javax.swing.JButton();
        sub_jTF = new javax.swing.JTextField();
        teacher_jTF = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        subjectData = new javax.swing.JTable();
        generateTtBtn = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        numHours_jTF = new javax.swing.JTextField();
        classroom_jTF = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        ttContainer = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel1.setText("Working Days");

        Tue_jCB.setText("Tuesday");
        Tue_jCB.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                Tue_jCBItemStateChanged(evt);
            }
        });

        Mon_jCB.setText("Monday");
        Mon_jCB.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                Mon_jCBItemStateChanged(evt);
            }
        });

        Wed_jCB.setText("Wednesday");
        Wed_jCB.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                Wed_jCBItemStateChanged(evt);
            }
        });

        Thu_jCB.setText("Thursday");
        Thu_jCB.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                Thu_jCBItemStateChanged(evt);
            }
        });

        Fri_jCB.setText("Friday");
        Fri_jCB.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                Fri_jCBItemStateChanged(evt);
            }
        });

        Sat_jCB.setText("Saturday");
        Sat_jCB.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                Sat_jCBItemStateChanged(evt);
            }
        });

        Sun_jCB.setText("Sunday");
        Sun_jCB.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                Sun_jCBItemStateChanged(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setText("Number of Hours");

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel3.setText("Per Day");

        MonHours_jTF.setText("0");
        MonHours_jTF.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MonHours_jTFActionPerformed(evt);
            }
        });

        TueHours_jTF.setText("0");

        WedHours_jTF.setText("0");

        ThuHours_jTF.setText("0");

        FriHours_jTF.setText("0");

        SatHours_jTF.setText("0");

        SunHours_jTF.setText("0");

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel4.setText("Subject");

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel5.setText("Teacher");

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel6.setText("Classroom");

        addBtn.setText("ADD");
        addBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addBtnActionPerformed(evt);
            }
        });

        teacher_jTF.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                teacher_jTFActionPerformed(evt);
            }
        });

        subjectData.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Subject", "Lectures Alloted", "Teacher", "Classroom"
            }
        ));
        jScrollPane1.setViewportView(subjectData);

        generateTtBtn.setText("GENERATE TIME TABLE");
        generateTtBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                generateTtBtnActionPerformed(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel7.setText("No. of Lectures");

        ttContainer.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(ttContainer);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.CENTER, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                        .addComponent(generateTtBtn)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 877, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.CENTER)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addGap(18, 18, 18)
                        .addComponent(sub_jTF, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel7)
                        .addGap(18, 18, 18)
                        .addComponent(numHours_jTF, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel5)
                        .addGap(18, 18, 18)
                        .addComponent(teacher_jTF, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel6)
                        .addGap(19, 19, 19)
                        .addComponent(classroom_jTF, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, Short.MAX_VALUE)
                        .addComponent(addBtn))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3)
                            .addComponent(jLabel1))
                        .addGap(38, 38, 38)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(Mon_jCB)
                            .addComponent(MonHours_jTF, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(35, 35, 35)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(Tue_jCB, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(TueHours_jTF))
                        .addGap(35, 35, 35)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(Wed_jCB, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(WedHours_jTF))
                        .addGap(35, 35, 35)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(Thu_jCB, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(ThuHours_jTF))
                        .addGap(35, 35, 35)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(Fri_jCB, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(FriHours_jTF, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(Sat_jCB, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(SatHours_jTF, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(Sun_jCB, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(SunHours_jTF, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(35, 35, 35))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(43, 43, 43)
                                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                                    .addComponent(Fri_jCB)
                                    .addComponent(Sat_jCB)
                                    .addComponent(Sun_jCB))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(FriHours_jTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(SatHours_jTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(SunHours_jTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 13, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                            .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, 31, Short.MAX_VALUE)
                            .addComponent(sub_jTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(addBtn)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(numHours_jTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(teacher_jTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addComponent(classroom_jTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(19, 19, 19))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                                    .addComponent(Mon_jCB)
                                    .addComponent(Tue_jCB)
                                    .addComponent(Wed_jCB)
                                    .addComponent(Thu_jCB))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(MonHours_jTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(TueHours_jTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(WedHours_jTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(ThuHours_jTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(generateTtBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(35, 35, 35)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 309, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(35, 35, 35))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void addBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addBtnActionPerformed
        // TODO add your handling code here:
        if("".equals(sub_jTF.getText()) || "".equals(teacher_jTF.getText()) || "".equals(classroom_jTF.getText()) || "".equals(numHours_jTF.getText()))
            JOptionPane.showMessageDialog(new JFrame(), "Please Enter All 4 Values!");
        else{
            ttInfo.setSubject(sub_jTF.getText());
            ttInfo.setTeacherName(teacher_jTF.getText());
            ttInfo.setClassroom(classroom_jTF.getText());
            ttInfo.setTimePerSubject(Integer.parseInt(numHours_jTF.getText()));

            fillTableData();
        }
    }//GEN-LAST:event_addBtnActionPerformed

    private void fillTableData() {
        ArrayList<String> subject = ttInfo.getSubject();
        ArrayList<String> teacherName = ttInfo.getTeacherName();
        ArrayList<String> classroom = ttInfo.getClassroom();
        ArrayList<Integer> timePerSubject = ttInfo.getTimePerSubject();
        
        int nRow = subject.size();
        int nCol = 4;
        
        Object[][] tableData = new Object[nRow][nCol];
        for(int i=0; i<nRow; i++){
            tableData[i][0] = subject.get(i);
            tableData[i][1] =  timePerSubject.get(i);
            tableData[i][2] = teacherName.get(i);
            tableData[i][3] =  classroom.get(i);
        }
        
        String colHeads[] = {"Subject","Lectures Alloted" ,"Teacher","Classroom" };
        DefaultTableModel myModel = new DefaultTableModel(tableData, colHeads){
            @Override
            public boolean isCellEditable(int row, int column){
                return false;
            }
        };
        
        subjectData.setModel(myModel);
    }
    
    private void generateTtBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_generateTtBtnActionPerformed
        // TODO add your handling code here:
        ttInfo.setHoursPerDay(Integer.parseInt(MonHours_jTF.getText()));
        ttInfo.setHoursPerDay(Integer.parseInt(TueHours_jTF.getText()));
        ttInfo.setHoursPerDay(Integer.parseInt(WedHours_jTF.getText()));
        ttInfo.setHoursPerDay(Integer.parseInt(ThuHours_jTF.getText()));
        ttInfo.setHoursPerDay(Integer.parseInt(FriHours_jTF.getText()));
        ttInfo.setHoursPerDay(Integer.parseInt(SatHours_jTF.getText()));
        ttInfo.setHoursPerDay(Integer.parseInt(SunHours_jTF.getText()));
        
        GenerateTimeTable tt = new GenerateTimeTable(ttInfo);
        DefaultTableModel myTable = tt.generateTt();
        ttContainer.setModel(myTable);
    }//GEN-LAST:event_generateTtBtnActionPerformed

    private void Mon_jCBItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_Mon_jCBItemStateChanged
        // TODO add your handling code here:
        ttInfo.setDays(evt, Mon_jCB.getText());
    }//GEN-LAST:event_Mon_jCBItemStateChanged

    private void Tue_jCBItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_Tue_jCBItemStateChanged
        // TODO add your handling code here:
        ttInfo.setDays(evt, Tue_jCB.getText());
    }//GEN-LAST:event_Tue_jCBItemStateChanged

    private void Wed_jCBItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_Wed_jCBItemStateChanged
        // TODO add your handling code here:
        ttInfo.setDays(evt, Wed_jCB.getText());
    }//GEN-LAST:event_Wed_jCBItemStateChanged

    private void Thu_jCBItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_Thu_jCBItemStateChanged
        // TODO add your handling code here:
        ttInfo.setDays(evt, Thu_jCB.getText());
    }//GEN-LAST:event_Thu_jCBItemStateChanged

    private void Fri_jCBItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_Fri_jCBItemStateChanged
        // TODO add your handling code here:
        ttInfo.setDays(evt, Fri_jCB.getText());
    }//GEN-LAST:event_Fri_jCBItemStateChanged

    private void Sat_jCBItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_Sat_jCBItemStateChanged
        // TODO add your handling code here:
        ttInfo.setDays(evt, Sat_jCB.getText());
    }//GEN-LAST:event_Sat_jCBItemStateChanged

    private void Sun_jCBItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_Sun_jCBItemStateChanged
        // TODO add your handling code here:
        ttInfo.setDays(evt, Sun_jCB.getText());
    }//GEN-LAST:event_Sun_jCBItemStateChanged

    private void MonHours_jTFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MonHours_jTFActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_MonHours_jTFActionPerformed

    private void teacher_jTFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_teacher_jTFActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_teacher_jTFActionPerformed

    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TimetableGenerator_UI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TimetableGenerator_UI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TimetableGenerator_UI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TimetableGenerator_UI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TimetableGenerator_UI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField FriHours_jTF;
    private javax.swing.JCheckBox Fri_jCB;
    private javax.swing.JTextField MonHours_jTF;
    private javax.swing.JCheckBox Mon_jCB;
    private javax.swing.JTextField SatHours_jTF;
    private javax.swing.JCheckBox Sat_jCB;
    private javax.swing.JTextField SunHours_jTF;
    private javax.swing.JCheckBox Sun_jCB;
    private javax.swing.JTextField ThuHours_jTF;
    private javax.swing.JCheckBox Thu_jCB;
    private javax.swing.JTextField TueHours_jTF;
    private javax.swing.JCheckBox Tue_jCB;
    private javax.swing.JTextField WedHours_jTF;
    private javax.swing.JCheckBox Wed_jCB;
    private javax.swing.JButton addBtn;
    private javax.swing.JTextField classroom_jTF;
    private javax.swing.JButton generateTtBtn;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextField numHours_jTF;
    private javax.swing.JTextField sub_jTF;
    private javax.swing.JTable subjectData;
    private javax.swing.JTextField teacher_jTF;
    private javax.swing.JTable ttContainer;
    // End of variables declaration//GEN-END:variables

}
