
import java.awt.event.ItemEvent;
import java.util.ArrayList;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class TimetableInfo {
    private ArrayList<String> days;
    private ArrayList<Integer> hoursPerDay;
    private ArrayList<String> subject;
    private ArrayList<String> teacherName;
    private ArrayList<String> classroom;
    private ArrayList<Integer> timePerSubject;
    
    TimetableInfo(){
        days = new ArrayList<>();
        hoursPerDay = new ArrayList<>();
        subject = new ArrayList<>();
        teacherName = new ArrayList<>();
        classroom = new ArrayList<>();
        timePerSubject = new ArrayList<>();
    }
    
    public ArrayList<String> getDays(){
        return days;
    }
    
    public ArrayList<Integer> getHoursPerDay(){
        return hoursPerDay;
    }
    
    public ArrayList<String> getSubject(){
        return subject;
    }
    
    public ArrayList<String> getTeacherName(){
        return teacherName;
    }
    
    public ArrayList<String> getClassroom(){
        return classroom;
    }
    
    public ArrayList<Integer> getTimePerSubject(){
        return timePerSubject;
    }
    
    void setDays(ItemEvent evt, String text){
        JCheckBox cb =  (JCheckBox)evt.getItem();
        if(cb.isSelected()){
            this.days.add(text);
        }
        else{
            this.days.remove(cb.getText());
        }
    }
    
    void setHoursPerDay(int hours){
        this.hoursPerDay.add(hours);
    }
    
    void setSubject(String text){
        if(subject.contains(text)){
            JOptionPane.showMessageDialog(new JFrame(), "This subject already exists");
        }
        else{
            this.subject.add(text);
        }
    }
    
    void setTeacherName(String text){
        this.teacherName.add(text);
    }
    
    void setClassroom(String text){
        this.classroom.add(text);
    }
    
    void setTimePerSubject(int time){
        this.timePerSubject.add(time);
    }
    
}
